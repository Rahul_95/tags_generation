#!flask/bin/python
from flask import Flask, jsonify
import random
app = Flask(__name__)


@app.route('/todo/api/v1.0/tasks/<string:title>/<string:categories>', methods=['GET'])
def get_tags(title,categories):
	Single_Categories = categories.split(",")
	str1 = ' '.join(Single_Categories)
	Categories = str1.split(" ")
	Title = title
	# Feature_Products = ['BR-V','Civic','Amaze','Jazz','CR-V','City','Accord']
	Help = ['toll free number','customer care','phone number','Offers','Contact Number']
	# Types = ['Sedan','Hatchback','SUV']


	Phrases = []
	for i in range (50):
	    Tag1 = random.choice(Categories)
	    Tag2 = random.choice(Single_Categories)+' near '+'<Locality>'
	    Tag3 = Title+" "+random.choice(Help)
	
	    Phrases.append(Tag1)
	    Phrases.append(Tag2)
	    Phrases.append(Tag3)
	    random.shuffle(Phrases)

	print("\n".join(list(set(Phrases))))
	return ("\n".join(list(set(Phrases))))


@app.route('/todo/api/v1.0/tasks/<string:title>/<string:categories>/<string:feature_products>', methods=['GET'])
def get_tags_feature_products(title,categories,feature_products):
	Single_Categories = categories.split(",")
	str1 = ' '.join(Single_Categories)
	Categories = str1.split(" ")
	Title = title
	Feature_Products = feature_products.split(",")
	Help = ['toll free number','customer care','phone number','Offers','Contact Number']
	# Types = ['Sedan','Hatchback','SUV']


	Phrases = []
	for i in range (50):
	    Tag1 = random.choice(Categories)
	    Tag2 = random.choice(Single_Categories)+' near '+'<Locality>'
	    Tag3 = Title+" "+random.choice(Help)
	    Tag4 = Title+" "+random.choice(Feature_Products)
	    Tag5 = Title+random.choice(Feature_Products)+" near <Locality>"
	    Tag6 = Title+random.choice(Feature_Products)+" in <City>"
	
	    Phrases.append(Tag1)
	    Phrases.append(Tag2)
	    Phrases.append(Tag3)
	    Phrases.append(Tag4)
	    Phrases.append(Tag5)
	    Phrases.append(Tag6)
	    random.shuffle(Phrases)

	print("\n".join(list(set(Phrases))))
	# print(category)
	return ("\n".join(list(set(Phrases))))

@app.route('/todo/api/v1.0/tasks/<string:title>/<string:categories>/<string:sub_categories>/<string:feature_products>/<string:locations>', methods=['GET'])
def get_tags_location(title,categories,sub_categories,feature_products,locations):
	if (locations == '""'):
		locations = '<Location>'
		print("========="+locations)
	print("Locations = "+locations)
	Single_Categories = categories.split(",")
	str1 = ' '.join(Single_Categories)
	Categories = str1.split(" ")
	Locations = locations.split(",")
	Sub_categories = sub_categories.split(",")
	Title = title
	Feature_Products = feature_products.split(",")
	Help = ['toll free number','customer care','phone number','Offers','Contact Number']
	# Types = ['Sedan','Hatchback','SUV']


	Phrases = []
	for i in range (50):
	    Tag1 = random.choice(Categories)
	    Tag2 = random.choice(Single_Categories)+' near '+ random.choice(Locations)
	    Tag3 = Title+" "+random.choice(Help)
	    Tag4 = Title+" "+random.choice(Feature_Products)
	    Tag5 = Title+random.choice(Feature_Products)+" near "+random.choice(Locations)
	    Tag6 = Title+random.choice(Feature_Products)+" in <City>"
	    Tag7 = Title+" "+random.choice(Sub_categories)
	
	    Phrases.append(Tag1)
	    Phrases.append(Tag2)
	    Phrases.append(Tag3)
	    Phrases.append(Tag4)
	    Phrases.append(Tag5)
	    Phrases.append(Tag6)
	    Phrases.append(Tag7)
	    random.shuffle(Phrases)

	print("\n".join(list(set(Phrases))))
	# print(category)
	return ("\n".join(list(set(Phrases))))



if __name__ == '__main__':
    app.run(debug=True)